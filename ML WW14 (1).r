#install packages
######
install.packages("VIM")
install.packages("corrplot")
install.packages("caret")
install.packages("caTools")
install.packages("ROSE")
install.packages('randomForest')
install.packages('e1071')
install.packages('rpart')
install.packages('rpart.plot')
install.packages('pROC')
install.packages('class')
install.packages('adabag')
install.packages('DMwR')
install.packages('Rserve')
install.packages("mvoutlier")
install.packages("maptools")
install.packages("MLmetrics")
install.packages("smotefamily")
library(smotefamily)
library(MLmetrics)
library(maptools)
library(Rserve)
#Rserve()
library(VIM)
library(corrplot)
library(caret)
library(caTools)
library(ROSE)
library(randomForest)
library(e1071)
library(rpart)
library(rpart.plot)
library(pROC)
library(class)
library(adabag)
library(DMwR)
library(dplyr)
library(xgboost)
library(magrittr)
library(Matrix)
#######
setwd("C:/Users/nadavste/Desktop/ml/time DF")
intel.raw<-read.csv("MLdataww13'20.csv")

names(intel.raw)[names(intel.raw) == "ï..is.Late"] <- "is.Late"
summary(intel.raw)
str(intel.raw)

table(intel.raw$is.hotfix)
table(intel.raw$is.Late)

df<-intel.raw
apply(df, 2, function(x) { sum(is.na(x)) })
cat("Row with no missing data:", sum(complete.cases(df)))   # 1012 rows with no missing data
cat("Row with missing data: ", sum(!complete.cases(df)))    # 49892 rows with missing data
df<- df[!is.na(df$PorYYYYWW),]
#***make sure to check in Hotfix without removing these rows
cat("Row with missing data: ", sum(!complete.cases(df)))    # 49892 rows with missing data
df[is.na(df)]<-0
cat("Row with missing data: ", sum(!complete.cases(df)))    # 49892 rows with missing data
str(df)
summary(df)
# make factor
#####
str(df)
df$is.Late <- as.factor(df$is.Late)
df$is.hotfix <- as.factor(df$is.hotfix)
df$is.missing.por <- as.factor(df$is.missing.por)
df$IsTC <- as.factor(df$IsTC)
df$TrendOffset <- as.factor(df$TrendOffset)
df$POR.Year <- as.factor(df$POR.Year)
df$Trend.Year <- as.factor(df$Trend.Year)
df$POR.WW <- as.factor(df$POR.WW)
df$Trend.WW <- as.factor(df$Trend.WW)
df$WW.before.milestone <- as.factor(df$WW.before.milestone)
df$LeadProduct.Type<-as.factor(df$LeadProduct.Type)
df$Configuration<-as.factor(df$Configuration)
################################
##              EDA           ##
################################
#WWB ->weeks7,9,13 look important for bugs. 2-6 a bit for features.
ggplot(intel.raw, aes(as.factor(intel.raw$WW.before.milestone),intel.raw$FunctionalPCR_5)) + geom_boxplot()+ylim(0,25)
#
summary(df$IP.Category)
summary(df$Configuration)
summary(df$LeadProduct)
levels(df$LeadProduct) <- c(levels(df$LeadProduct), "Missing") 
df$LeadProduct[df$LeadProduct=='""']  <- "Missing"
df$LeadProduct<-factor(df$LeadProduct)

summary(df$IpFamily)
summary(df$Generation)
summary(df$Lifecycle)
summary(df$IpDevelopmentLead)
levels(df$IpDevelopmentLead) <- c(levels(df$IpDevelopmentLead), "Missing") 
df$IpDevelopmentLead[df$IpDevelopmentLead=='""']  <- "Missing"
df$IpDevelopmentLead<-factor(df$IpDevelopmentLead)

summary(df$Revision)################
levels(df$Revision) <- c(levels(df$Revision), "Missing") 
df$Revision[df$Revision=='""']  <- "Missing"
df$Revision<-factor(df$Revision)

ggplot(df, aes(df$WW.before.milestone,fill = df$is.Late)) + geom_histogram()
barplot(sort(prop.table(table(df$Configuration)),decreasing=T))
barplot(sort(prop.table(table(df$LeadProduct)),decreasing=T))
barplot(sort(prop.table(table(df$LeadProduct.Type)),decreasing=T))
levels(df$LeadProduct.Type) <- c(levels(df$LeadProduct.Type), "Missing") 
df$LeadProduct.Type[df$LeadProduct.Type=='""']  <- "Missing"
df$LeadProduct.Type[df$LeadProduct.Type=='']  <- "Missing"
summary(df$LeadProduct.Type)
df$LeadProduct.Type<-factor(df$LeadProduct.Type)
summary(df$Revision[df$Revision=='other'])
barplot(sort(prop.table(table(df$Revision)),decreasing=T))
max(df$FunctionalPCR_1)


#####removing columns that have ireelivent or leackage in them
df$ï..IpConfigurationId<-NULL
df$IsSubIp<-NULL
df$key<-NULL
df$KEY2<-NULL
#df$MilestoneId<-NULL
df$TrendYYYYWW<-NULL
df$PorYYYYWW<-NULL
df$is.missing.trend<-NULL
#
df$TrendOffset<-NULL
df$Trend.Year<-NULL
df$POR.Year<-NULL
df$POR.WW<-NULL
df$Trend.WW<-NULL
df$is.missing.por<-NULL
df$IpDevelopmentLead<-NULL
df$Revision<-NULL
df$WW<-NULL
df$Revision<-NULL

#write.csv(df,"C:/Users/nadavste/Desktop/ml/\\FullData_NoNA.csv", row.names = FALSE)
compare.model.Late<-c(1,1,1,1,1,1)
names(compare.model.Late)<-c("Model","Data solution","Precision","Recall","AUC","F1")

############################################################################################
#                                              LATE                                        #
############################################################################################
table(df$is.Late)
#----------------------------------------------------
#solutions for imbalanced Data 
#----------------------------------------------------
str(df)
df_rf<-df
set.seed(123)

filter <- sample.split(df_rf$is.Late ,group = df_rf$MilestoneId, SplitRatio = 0.7)#????????
df_train <- subset(df_rf, filter ==T)
df_test <- subset(df_rf, filter ==F)

table(df_train$is.Late)
table(df_test$is.Late)
#View(df_train)
#View(df_test)

#write.csv(df_train,"C:/Users/nadavste/Desktop/ml/\\df_trainGROUP.csv", row.names = FALSE)
#write.csv(df_test,"C:/Users/nadavste/Desktop/ml/\\df_testGROUP.csv", row.names = FALSE)

df_train$MilestoneId<-NULL
df_test$MilestoneId<-NULL


#Oversampeling
over<- ovun.sample(is.Late~. ,data=df_train, method='over' , N= 28675)$data
#undersampling
under<- ovun.sample(is.Late~. ,data=df_train, method='under' , N= 5900)$data
#both
both<- ovun.sample(is.Late~. ,data=df_train, method='both' , N=17000)$data
#smote
smote<-SMOTE(is.Late~ ., df_train, perc.over = 1000, k = 5, perc.under = 500)
#ADASYN
#??adas
#ADAS(as.numeric(df_train$is.hotfix),as.numeric(df_train$is.Late),K=5)
str(df_rf)
table(df_rf$is.Late)
table(over$is.Late)
table(under$is.Late)
table(both$is.Late)
table(smote$is.Late)

#-----------------------------------------------------------------------------
##                              modelRandom                                 ##
#-----------------------------------------------------------------------------
#####
#----------------------------------------------------data clean
####
str(over)
#remove Factor with more then 53 categories:

#
df_trainRF<-df_train
modelRF <- randomForest(is.Late~.,data = df_trainRF , importance=TRUE, na.action=na.exclude)
prediction.RF<-predict(modelRF,df_test)
actual.rf <- df_test$is.Late
cf.rf <- table(prediction.RF,actual.rf)
precision.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[2,1])
recall.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[1,2])
cf.rf 
precision.rf
recall.rf
F1.RF<-F1_Score(prediction.RF,actual.rf, positive = '1')
acc<-Accuracy(prediction.RF, actual.rf)
#AUC for Random Forest
predict.prob.rf <- predict(modelRF,type="prob",df_test)[,2]
library(ROCR)
Pred.Prob.RF<-prediction(predict.prob.rf,df_test$is.Late)
perf.RF.clean<-performance(Pred.Prob.RF,"tpr","fpr")
plot(perf.RF.clean,main = "ROC Curve for Random Forest",col=3,lwd=2)
abline(a=0,b=1,lwd=2,lty=2,col="gray")
auc <- performance(Pred.Prob.RF,"auc")
#auc <- AUC(prediction.RF, actual.rf) might be the correct way to calculate AUC need to varify*****************
rocRF.clean <- unlist(slot(auc, "y.values"))

RF_clean<-c("RF","clean",precision.rf,recall.rf,rocRF.clean,F1.RF)
compare.model.Late<-rbind(compare.model.Late,RF_clean)
####
#-----------------------------------------------------data over
####
df_trainRF<-over
modelRF <- randomForest(is.Late~.,data = df_trainRF , importance=TRUE, na.action=na.exclude)
prediction.RF<-predict(modelRF,df_test)
actual.rf <- df_test$is.Late
cf.rf <- table(prediction.RF,actual.rf)
precision.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[2,1])
recall.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[1,2])
cf.rf 
precision.rf
recall.rf
F1.RF<-F1_Score(actual.rf, prediction.RF, positive = '1')
#AUC for Random Forest
predict.prob.rf <- predict(modelRF,type="prob",df_test)[,2]
library(ROCR)
Pred.Prob.RF<-prediction(predict.prob.rf,df_test$is.Late)
perf.RF.clean<-performance(Pred.Prob.RF,"tpr","fpr")
plot(perf.RF.clean,main = "ROC Curve for Random Forest",col=3,lwd=2)
abline(a=0,b=1,lwd=2,lty=2,col="gray")
auc <- performance(Pred.Prob.RF,"auc")
rocRF.over <- unlist(slot(auc, "y.values"))

RF_over<-c("RF","over",precision.rf,recall.rf,rocRF.over,F1.RF)
compare.model.Late<-rbind(compare.model.Late,RF_over)
###
#----------------------------------------------------------data under
###
df_trainRF<-under
modelRF <- randomForest(is.Late~.,data = df_trainRF , importance=TRUE, na.action=na.exclude)
prediction.RF<-predict(modelRF,df_test)
actual.rf <- df_test$is.Late
cf.rf <- table(prediction.RF,actual.rf)
precision.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[2,1])
recall.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[1,2])
cf.rf 
precision.rf
recall.rf
F1.RF<-F1_Score(actual.rf, prediction.RF, positive = '1')
#AUC for Random Forest
predict.prob.rf <- predict(modelRF,type="prob",df_test)[,2]
library(ROCR)
Pred.Prob.RF<-prediction(predict.prob.rf,df_test$is.Late)
perf.RF.clean<-performance(Pred.Prob.RF,"tpr","fpr")
plot(perf.RF.clean,main = "ROC Curve for Random Forest",col=3,lwd=2)
abline(a=0,b=1,lwd=2,lty=2,col="gray")
auc <- performance(Pred.Prob.RF,"auc")
rocRF.under <- unlist(slot(auc, "y.values"))

RF_under<-c("RF","under",precision.rf,recall.rf,rocRF.under,F1.RF)
compare.model.Late<-rbind(compare.model.Late,RF_under)
###
#------------------------------------------------------------data both
###
df_trainRF<-both
modelRF <- randomForest(is.Late~.,data = df_trainRF , importance=TRUE, na.action=na.exclude)
prediction.RF<-predict(modelRF,df_test)
actual.rf <- df_test$is.Late
cf.rf <- table(prediction.RF,actual.rf)
precision.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[2,1])
recall.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[1,2])
cf.rf 
precision.rf
recall.rf
F1.RF<-F1_Score(actual.rf, prediction.RF, positive = '1')
#AUC for Random Forest
predict.prob.rf <- predict(modelRF,type="prob",df_test)[,2]
library(ROCR)
Pred.Prob.RF<-prediction(predict.prob.rf,df_test$is.Late)
perf.RF.clean<-performance(Pred.Prob.RF,"tpr","fpr")
plot(perf.RF.clean,main = "ROC Curve for Random Forest",col=3,lwd=2)
abline(a=0,b=1,lwd=2,lty=2,col="gray")
auc <- performance(Pred.Prob.RF,"auc")
rocRF.both <- unlist(slot(auc, "y.values"))

RF_both<-c("RF","both",precision.rf,recall.rf,rocRF.both,F1.RF)
compare.model.Late<-rbind(compare.model.Late,RF_both)
####
#--------------------------------------------------------------data smote
####
df_trainRF<-smote
modelRF <- randomForest(is.Late~.,data = df_trainRF , importance=TRUE, na.action=na.exclude)
prediction.RF<-predict(modelRF,df_test)
actual.rf <- df_test$is.Late
cf.rf <- table(prediction.RF,actual.rf)
precision.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[2,1])
recall.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[1,2])
cf.rf 
precision.rf
recall.rf
F1.RF<-F1_Score(actual.rf, prediction.RF, positive = '1')
#AUC for Random Forest
predict.prob.rf <- predict(modelRF,type="prob",df_test)[,2]
library(ROCR)
Pred.Prob.RF<-prediction(predict.prob.rf,df_test$is.Late)
perf.RF.clean<-performance(Pred.Prob.RF,"tpr","fpr")
plot(perf.RF.clean,main = "ROC Curve for Random Forest",col=3,lwd=2)
abline(a=0,b=1,lwd=2,lty=2,col="gray")
auc <- performance(Pred.Prob.RF,"auc")
rocRF.SMOTE <- unlist(slot(auc, "y.values"))

RF_smote<-c("RF","smote",precision.rf,recall.rf,rocRF.SMOTE,F1.RF)
compare.model.Late<-rbind(compare.model.Late,RF_smote)

#-----------------------------------------------------------------------------
##                              naiveBayes                                  ##
#-----------------------------------------------------------------------------
#####
library(e1071)
#the prediction probebility P
P<-0.5

str(df_train)
df_nb<-over
samp_size = nrow(df_nb[df_nb$is.Late==1,])
table(df_nb$is.Late)
dim(df_nb)
d<-dim(df_nb)[2]

model_NB <- naiveBayes(df_nb[,-1],df_nb$is.Late)
prediction_NB <- predict(model_NB,df_test[,-1], type = 'raw')
prediction_bankrupt_NB <- prediction_NB[,"1"]
actual_NB <- df_test$is.Late
predicted_NB <- prediction_bankrupt_NB > P #×× ××
cf_NB <- table(predicted_NB,actual_NB)
precision_NB <- cf_NB[2,2]/(cf_NB[2,2] + cf_NB[2,1])
recall_NB <- cf_NB[2,2]/(cf_NB[2,2] + cf_NB[1,2])
cf_NB
precision_NB
recall_NB
rocNB.clean <-roc(df_test$is.Late , prediction_bankrupt_NB, direction = "<", levels = c('0','1'))
auc(rocNB.clean)

summary(model_NB)

#-----------------------------------------------------------------------------
##                               XGBoost                                    ##
#-----------------------------------------------------------------------------
#לפתוח רק את הראשון
#----
###

#-----------------------------------------------------------------------clean
###
df_xgBoost_hasNA_train<-df_train
df_xgBoost_hasNA_test<-df_test
df_XG<-df_xgBoost_hasNA_train#**************************
df_XG$is.Late<-as.integer(df_XG$is.Late)-1
table(df_XG$is.Late)

trainm<-sparse.model.matrix(is.Late~.-1, data=df_XG)
#length(df_XG$is.Late)
train_lable<-df_XG[,"is.Late"]
train_matrix<-xgb.DMatrix(data=as.matrix(trainm), label = train_lable)
#
df_test_xg<-df_xgBoost_hasNA_test#****************************
df_test_xg$is.Late<-as.integer(df_test_xg$is.Late)-1
testm <-sparse.model.matrix(is.Late~.-1,data=df_test_xg)
test_lable<- df_test_xg[,"is.Late"]
table(test_lable)
test_matrix<-xgb.DMatrix(data=as.matrix(testm), label = test_lable)
nc <-length(unique(train_lable))

#Parameters-Params and CV not ready yet

#----
library(xgboost)
library(ggplot2)
library(reshape2)
install.packages('Ecdat')
library(Ecdat)
set.seed(123)

# = parameters = #
#----
# = eta candidates = #
eta=c(0.05,0.01,0.5)
# = colsample_bylevel candidates = #
cs=c(1/3,2/3,1)
# = max_depth candidates = #
md=c(2,4,6,10)
# = sub_sample candidates = #
ss=c(0.25,0.5,0.75,1)
# = standard model is the second value  of each vector above = #
standard=c(2,2,3,2)
#grid search CV
searchGridSubCol<-expand.grid(subsample = c(0.5, 0.6), 
                              colsample_bytree = c(0.5, 0.6),
                              max_depth = c(3, 4),
                              min_child = seq(1), 
                              eta = c(0.1)
)
ntrees <- 100
dtrain<-train_matrix
dtest<-test_matrix
system.time(
  rmseErrorsHyperparameters <- apply(searchGridSubCol, 1, function(parameterList){
    
    #Extract Parameters to test
    currentSubsampleRate <- parameterList[["subsample"]]
    currentColsampleRate <- parameterList[["colsample_bytree"]]
    currentDepth <- parameterList[["max_depth"]]
    currentEta <- parameterList[["eta"]]
    currentMinChild <- parameterList[["min_child"]]
    xgboostModelCV <- xgb.cv(data =  dtrain, nrounds = ntrees, nfold = 5, showsd = TRUE, 
                             metrics = "rmse", verbose = TRUE, "eval_metric" = "rmse",
                             "objective" = "reg:linear", "max.depth" = currentDepth, "eta" = currentEta,                               
                             "subsample" = currentSubsampleRate, "colsample_bytree" = currentColsampleRate
                             , print_every_n = 10, "min_child_weight" = currentMinChild, booster = "gbtree",
                             early_stopping_rounds = 10)
    
    xvalidationScores <- as.data.frame(xgboostModelCV$evaluation_log)
    rmse <- tail(xvalidationScores$test_rmse_mean, 1)
    trmse <- tail(xvalidationScores$train_rmse_mean,1)
    output <- return(c(rmse, trmse, currentSubsampleRate, currentColsampleRate, currentDepth, currentEta, currentMinChild))}))

# = train and test data = #
xtrain = as.matrix(trainm)
ytrain = as.matrix(train_lable)
xtest = as.matrix(testm)
ytest = as.matrix(test_lable)

conv_eta = matrix(NA,500,length(eta))
pred_eta = matrix(NA,length(df_test_xg), length(eta))
colnames(conv_eta) = colnames(pred_eta) = eta
for(i in 1:length(eta)){
  params=list(eta = eta[i], colsample_bylevel=cs[standard[2]],
              subsample = ss[standard[4]], max_depth = md[standard[3]],
              min_child_weigth = 1)
  xgb=xgboost(data = xtrain, label = ytrain, nrounds = 500, params = params)
  conv_eta[,i] = xgb$evaluation_log$train_rmse
  pred_eta[,i] = predict(xgb, test_matrix)
}
#
conv_eta = data.frame(iter=1:500, conv_eta)
conv_eta = melt(conv_eta, id.vars = "iter")
ggplot(data = conv_eta) + geom_line(aes(x = iter, y = value, color = variable))

#-------------------------------


xgb_params<-list("objective" = "multi:softprob","eval_metric" = "mlogloss", "num_class"=nc,
                    eta = 0.01,max.depth = 5#,gamma = 3,subsample = 0.75, colsample_bytree = 1
                      )
watchlist<-list(train = train_matrix , test= test_matrix)
#test 
model.xgb <-xgb.train(params = xgb_params, data = train_matrix,nthreads=1,nrounds=200, early_stopping_rounds=10,verbose=0,watchlist = watchlist)
#cross
e<- data.frame(model.xgb$evaluation_log)
plot(e$iter, e$train_mlogloss, col = 'blue')
lines(e$iter, e$test_mlogloss, col = 'red')
min(e$test_mlogloss)
#feature importance
imp<-xgb.importance(colnames(train_matrix) , model = model.xgb)
print(imp)
xgb.plot.importance(imp)
#predictions
p<-predict(model.xgb, newdata = test_matrix)
aa<-head(p)
aa[3]+ aa[4]
length(p)/2
pred <-matrix(p,nrow=nc,ncol=length(p)/(nc)) %>%
  t()%>%
  data.frame()%>%
  mutate(label = test_lable, max_prob = max.col(.,"last")-1)
head(pred)

###****more scores if needed
#cm<-confusionMatrix(as.factor(pred$max_prob), as.factor(pred$label),positive = '1')
#####
n = sum(cm) # number of instances
nc = nrow(cm) # number of classes
diag = diag(cm) # number of correctly classified instances per class 
rowsums = apply(cm, 1, sum) # number of instances per class
colsums = apply(cm, 2, sum) # number of predictions per class
p = rowsums / n # distribution of instances over the actual classes
q = colsums / n # distribution of instances over the predicted classes
precision = diag / colsums 
recall = diag / rowsums 
f1 = 2 * precision * recall / (precision + recall) 
data.frame(precision, recall, f1)
#####
#cf_xgb$byClass["Recall"]
#fourfoldplot(cf_xgb$table)
install.packages('yardstick')
library(yardstick)



# The confusion matrix from a single assessment set (i.e. fold)
cm_d <- as.data.frame(cm$table)
# confusion matrix statistics as data.frame
cm_st <-data.frame(cm$overall)
# round the values
cm_st$cm.overall <- round(cm_st$cm.overall,2)

# here we also have the rounded percentage values
cm_p <- as.data.frame(prop.table(cm$table))
cm_d$Perc <- round(cm_p$Freq*100,2)
# plotting the matrix
cm_d_p <-  ggplot(data = cm_d, aes(x = Prediction , y =  Reference, fill = Freq))+
  geom_tile() +
  geom_text(aes(label = paste("",Freq,",",Perc,"%")), color = 'red', size = 8) +
  theme_light() +
  guides(fill=FALSE) 

# plotting the stats
cm_st_p <-  tableGrob(cm_st)

# all together
grid.arrange(cm_d_p, cm_st_p,nrow = 1, ncol = 2, 
             top=textGrob("Confusion Matrix and Statistics",gp=gpar(fontsize=5,font=1)))
#----
####*****
cf_xgb<-table(Prediction = pred$max_prob , Actual = pred$label)
precision.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[2,1])
recall.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[1,2])
cf_xgb
precision.xgb
recall.xgb
F1.XG<-F1_Score(pred$label, pred$max_prob, positive = '1')

rocXG.clean <-roc(df_test$is.Late , pred$max_prob, direction = "<", levels = c('0','1'))
auc(rocXG.clean)
XG_clean<-c("XGBoost","clean",precision.xgb,recall.xgb,auc(rocXG.clean),F1.XG)
compare.model.Late<-rbind(compare.model.Late,XG_clean)
###
#----------------------------------------------------------------------------over
###
df_xgBoost_hasNA_train<-over
df_xgBoost_hasNA_test<-df_test
df_XG<-df_xgBoost_hasNA_train#**************************
df_XG$is.Late<-as.integer(df_XG$is.Late)-1

trainm<-sparse.model.matrix(is.Late~.-1, data=df_XG)
length(df_XG$is.Late)
train_lable<-df_XG[,"is.Late"]
length(train_lable)/length(colnames(train_lable))
train_matrix<-xgb.DMatrix(data=as.matrix(trainm), label = train_lable)
#
df_test_xg<-df_xgBoost_hasNA_test#****************************
df_test_xg$is.Late<-as.integer(df_test_xg$is.Late)-1
testm <-sparse.model.matrix(is.Late~.-1,data=df_test_xg)
test_lable<- df_test_xg[,"is.Late"]
test_matrix<-xgb.DMatrix(data=as.matrix(testm), label = test_lable)
nc <-length(unique(train_lable))
xgb_params<-list("objective" = "multi:softprob","eval_metric" = "mlogloss", "num_class"=nc)
watchlist<-list(train = train_matrix , test= test_matrix)
#test
model.xgb <-xgb.train(params = xgb_params, data = train_matrix, nrounds = 100, watchlist = watchlist,eta=0.2,max.depth = 3,gamma = 0, subsample = 1, colsample_bytree = 1, missing = NA , seed = 55)
e<- data.frame(model.xgb$evaluation_log)
plot(e$iter, e$train_mlogloss, col = 'blue')
lines(e$iter, e$test_mlogloss, col = 'red')
min(e$test_mlogloss)
#feature importance
imp<-xgb.importance(colnames(train_matrix) , model = model.xgb)
print(imp)
xgb.plot.importance(imp)
#predictions
summary(model.xgb)
p<-predict(model.xgb, newdata = test_matrix)
aa<-head(p)
aa[3]+ aa[4]
length(p)/2
pred <-matrix(p,nrow=nc,ncol=length(p)/(nc)) %>%
  t()%>%
  data.frame()%>%
  mutate(label = test_lable, max_prob = max.col(.,"last")-1)
head(pred)
cf_xgb<-table(Prediction = pred$max_prob , Actual = pred$label)
precision.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[2,1])
recall.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[1,2])
cf_xgb
precision.xgb
recall.xgb
F1.XG<-F1_Score(pred$label, pred$max_prob, positive = '1')

rocXG.over <-roc(df_test$is.Late , pred$max_prob, direction = "<", levels = c('0','1'))
auc(rocXG.over)
XG_over<-c("XGBoost","over",precision.xgb,recall.xgb,auc(rocXG.over),F1.XG)
compare.model.Late<-rbind(compare.model.Late,XG_over)
###
#-----------------------------------------------------------------------under
###
df_xgBoost_hasNA_train<-under
df_xgBoost_hasNA_test<-df_test
df_XG<-df_xgBoost_hasNA_train#**************************
df_XG$is.Late<-as.integer(df_XG$is.Late)-1

trainm<-sparse.model.matrix(is.Late~.-1, data=df_XG)
length(df_XG$is.Late)
train_lable<-df_XG[,"is.Late"]
length(train_lable)/length(colnames(train_lable))
train_matrix<-xgb.DMatrix(data=as.matrix(trainm), label = train_lable)
#
df_test_xg<-df_xgBoost_hasNA_test#****************************
df_test_xg$is.Late<-as.integer(df_test_xg$is.Late)-1
testm <-sparse.model.matrix(is.Late~.-1,data=df_test_xg)
test_lable<- df_test_xg[,"is.Late"]
test_matrix<-xgb.DMatrix(data=as.matrix(testm), label = test_lable)
nc <-length(unique(train_lable))
xgb_params<-list("objective" = "multi:softprob","eval_metric" = "mlogloss", "num_class"=nc)
watchlist<-list(train = train_matrix , test= test_matrix)
#test
model.xgb <-xgb.train(params = xgb_params, data = train_matrix, nrounds = 50, watchlist = watchlist,eta=0.5,max.depth = 6,gamma = 0, subsample = 1, colsample_bytree = 1, missing = NA , seed = 55)
e<- data.frame(model.xgb$evaluation_log)
plot(e$iter, e$train_mlogloss, col = 'blue')
lines(e$iter, e$test_mlogloss, col = 'red')
min(e$test_mlogloss)
#feature importance
imp<-xgb.importance(colnames(train_matrix) , model = model.xgb)
print(imp)
xgb.plot.importance(imp)
#predictions
p<-predict(model.xgb, newdata = test_matrix)
aa<-head(p)
aa[3]+ aa[4]
length(p)/2
pred <-matrix(p,nrow=nc,ncol=length(p)/(nc)) %>%
  t()%>%
  data.frame()%>%
  mutate(label = test_lable, max_prob = max.col(.,"last")-1)
head(pred)
cf_xgb<-table(Prediction = pred$max_prob , Actual = pred$label)
precision.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[2,1])
recall.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[1,2])
cf_xgb
precision.xgb
recall.xgb
F1.XG<-F1_Score(pred$label, pred$max_prob, positive = '1')

rocXG.under <-roc(df_test$is.Late , pred$max_prob, direction = "<", levels = c('0','1'))
auc(rocXG.under)
XG_under<-c("XGBoost","under",precision.xgb,recall.xgb,auc(rocXG.under),F1.XG)
compare.model.Late<-rbind(compare.model.Late,XG_under)
###
#-------------------------------------------------------------------------both
###
df_xgBoost_hasNA_train<-both
df_xgBoost_hasNA_test<-df_test
df_XG<-df_xgBoost_hasNA_train#**************************
df_XG$is.Late<-as.integer(df_XG$is.Late)-1

#options(na.action = 'na.pass') #############!!!!!!!!!!!!!!!!!!!!!!! alert!!!!!!!
trainm<-sparse.model.matrix(is.Late~.-1, data=df_XG)
length(df_XG$is.Late)
train_lable<-df_XG[,"is.Late"]
length(train_lable)/length(colnames(train_lable))
train_matrix<-xgb.DMatrix(data=as.matrix(trainm), label = train_lable)
#
df_test_xg<-df_xgBoost_hasNA_test#****************************
df_test_xg$is.Late<-as.integer(df_test_xg$is.Late)-1
#options(na.action = 'na.fail') #############!!!!!!!!!!!!!!!!!!!!!!! alert!!!!!!!
testm <-sparse.model.matrix(is.Late~.-1,data=df_test_xg)
test_lable<- df_test_xg[,"is.Late"]
test_matrix<-xgb.DMatrix(data=as.matrix(testm), label = test_lable)
nc <-length(unique(train_lable))
xgb_params<-list("objective" = "multi:softprob","eval_metric" = "mlogloss", "num_class"=nc)
watchlist<-list(train = train_matrix , test= test_matrix)
#test
model.xgb <-xgb.train(params = xgb_params, data = train_matrix, nrounds = 100, watchlist = watchlist,eta=0.5,max.depth = 10,lamda = 1, subsample = 0.8, colsample_bytree = 1, missing = NA , seed = 55)
e<- data.frame(model.xgb$evaluation_log)
plot(e$iter, e$train_mlogloss, col = 'blue')
lines(e$iter, e$test_mlogloss, col = 'red')
min(e$test_mlogloss)
#feature importance
imp<-xgb.importance(colnames(train_matrix) , model = model.xgb)
print(imp)
xgb.plot.importance(imp)
#predictions
p<-predict(model.xgb, newdata = test_matrix)
aa<-head(p)
aa[3]+ aa[4]
length(p)/2
pred <-matrix(p,nrow=nc,ncol=length(p)/(nc)) %>%
  t()%>%
  data.frame()%>%
  mutate(label = test_lable, max_prob = max.col(.,"last")-1)
head(pred)
cf_xgb<-table(Prediction = pred$max_prob , Actual = pred$label)
precision.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[2,1])
recall.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[1,2])
cf_xgb
precision.xgb
recall.xgb
F1.XG<-F1_Score(pred$label, pred$max_prob, positive = '1')

rocXG.both <-roc(df_test$is.Late , pred$max_prob, direction = "<", levels = c('0','1'))
auc(rocXG.both)
XG_both<-c("XGBoost","both",precision.xgb,recall.xgb,auc(rocXG.both),F1.XG)
compare.model.Late<-rbind(compare.model.Late,XG_both)
###
#------------------------------------------------------------------------smote
###
df_xgBoost_hasNA_train<-smote
df_xgBoost_hasNA_test<-df_test
df_XG<-df_xgBoost_hasNA_train#**************************
df_XG$is.Late<-as.integer(df_XG$is.Late)-1

#options(na.action = 'na.pass') #############!!!!!!!!!!!!!!!!!!!!!!! alert!!!!!!!
trainm<-sparse.model.matrix(is.Late~.-1, data=df_XG)
length(df_XG$is.Late)
train_lable<-df_XG[,"is.Late"]
length(train_lable)/length(colnames(train_lable))
train_matrix<-xgb.DMatrix(data=as.matrix(trainm), label = train_lable)
#
df_test_xg<-df_xgBoost_hasNA_test#****************************
df_test_xg$is.Late<-as.integer(df_test_xg$is.Late)-1
#options(na.action = 'na.fail') #############!!!!!!!!!!!!!!!!!!!!!!! alert!!!!!!!
testm <-sparse.model.matrix(is.Late~.-1,data=df_test_xg)
test_lable<- df_test_xg[,"is.Late"]
test_matrix<-xgb.DMatrix(data=as.matrix(testm), label = test_lable)
nc <-length(unique(train_lable))
xgb_params<-list("objective" = "multi:softprob","eval_metric" = "mlogloss", "num_class"=nc)
watchlist<-list(train = train_matrix , test= test_matrix)
#test
model.xgb <-xgb.train(params = xgb_params, data = train_matrix, nrounds = 100, watchlist = watchlist,eta=0.5,max.depth = 6,gamma = 0, subsample = 1, colsample_bytree = 1, missing = NA , seed = 55)
e<- data.frame(model.xgb$evaluation_log)
plot(e$iter, e$train_mlogloss, col = 'blue')
lines(e$iter, e$test_mlogloss, col = 'red')
min(e$test_mlogloss)
#feature importance
imp<-xgb.importance(colnames(train_matrix) , model = model.xgb)
print(imp)
xgb.plot.importance(imp)
#predictions
p<-predict(model.xgb, newdata = test_matrix)
aa<-head(p)
aa[3]+ aa[4]
length(p)/2
pred <-matrix(p,nrow=nc,ncol=length(p)/(nc)) %>%
  t()%>%
  data.frame()%>%
  mutate(label = test_lable, max_prob = max.col(.,"last")-1)
head(pred)
cf_xgb<-table(Prediction = pred$max_prob , Actual = pred$label)
precision.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[2,1])
recall.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[1,2])
cf_xgb
precision.xgb
recall.xgb
F1.XG<-F1_Score(pred$label, pred$max_prob, positive = '1')

rocXG.SMOTE <-roc(df_test$is.Late , pred$max_prob, direction = "<", levels = c('0','1'))
auc(rocXG.SMOTE)
XG_smote<-c("XGBoost","smote",precision.xgb,recall.xgb,auc(rocXG.SMOTE),F1.XG)
compare.model.Late<-rbind(compare.model.Late,XG_smote)
#-----------------------------------------------------------------------------
##                                DTree                                     ##
#-----------------------------------------------------------------------------
#####
P<-0.5
#----------------------------------------------------clean

df_trainRP<-df_train
df_trainRP$DropType<-NULL
df_trainRP$IP.Category<-NULL
df_trainRP$is.hotfix<-NULL
df_trainRP$LeadProduct.Type<-NULL
modelRP <- rpart(is.Late~.,df_trainRP,cp=0.007)
rpart.plot(modelRP, box.palette = "RdBu", shadow.col = "grey", nn=TRUE,fallen.leaves = FALSE)
predict.prob_rp <- predict(modelRP,df_test)
predict.prob_rp<-predict.prob_rp[,2]
prediction_rp <- predict.prob_rp >P #×× ××
actual_rp <- df_test$is.Late
cf_rp <- table(prediction_rp,actual_rp)
precision_rp <- cf_rp[2,2]/(cf_rp[2,2] + cf_rp[1,2])
recall_rp <- cf_rp[2,2]/(cf_rp[2,2] + cf_rp[2,1])
cf_rp
precision_rp
recall_rp
F1.DT<-"x"
x<-F1_Score(actual_rp, predict.prob_rp, positive = '1')

rocDT.clean <-roc(df_test$is.Late , predict.prob_rp, direction = "<", levels = c('0','1'))
auc(rocDT.clean)
DT_clean<-c("DT","clean",precision_rp,recall_rp,auc(rocDT.clean),F1.DT)
compare.model.Late<-rbind(compare.model.Late,DT_clean)
#----------------------------------------------------over
df_trainRP<-over
modelRP <- rpart(is.Late~.,df_trainRP)
rpart.plot(modelRP, box.palette = "RdBu", shadow.col = "grey", nn=TRUE)
predict.prob_rp <- predict(modelRP,df_test)
predict.prob_rp<-predict.prob_rp[,2]
prediction_rp <- predict.prob_rp >P #×× ××
actual_rp <- df_test$is.Late
cf_rp <- table(prediction_rp,actual_rp)
precision_rp <- cf_rp[2,2]/(cf_rp[2,2] + cf_rp[1,2])
recall_rp <- cf_rp[2,2]/(cf_rp[2,2] + cf_rp[2,1])
cf_rp
precision_rp
recall_rp
F1.DT<-F1_Score(actual_rp, predict.prob_rp, positive = '1')

rocDT.over <-roc(df_test$is.Late , predict.prob_rp, direction = "<", levels = c('0','1'))
auc(rocDT.over)
DT_over<-c("DT","over",precision_rp,recall_rp,auc(rocDT.over),F1.DT)
compare.model.Late<-rbind(compare.model.Late,DT_over)
#----------------------------------------------------under
df_trainRP<-under
modelRP <- rpart(is.Late~.,df_trainRP)
rpart.plot(modelRP, box.palette = "RdBu", shadow.col = "grey", nn=TRUE)
predict.prob_rp <- predict(modelRP,df_test)
predict.prob_rp<-predict.prob_rp[,2]
prediction_rp <- predict.prob_rp >P #×× ××
actual_rp <- df_test$is.Late
cf_rp <- table(prediction_rp,actual_rp)
precision_rp <- cf_rp[2,2]/(cf_rp[2,2] + cf_rp[1,2])
recall_rp <- cf_rp[2,2]/(cf_rp[2,2] + cf_rp[2,1])
cf_rp
precision_rp
recall_rp
F1.DT<-F1_Score(actual_rp, predict.prob_rp, positive = '1')

rocDT.under <-roc(df_test$is.Late , predict.prob_rp, direction = "<", levels = c('0','1'))
auc(rocDT.under)
DT_under<-c("DT","under",precision_rp,recall_rp,auc(rocDT.under),F1.DT)
compare.model.Late<-rbind(compare.model.Late,DT_under)
#----------------------------------------------------both
df_trainRP<-both
modelRP <- rpart(is.Late~.,df_trainRP)
rpart.plot(modelRP, box.palette = "RdBu", shadow.col = "grey", nn=TRUE)
predict.prob_rp <- predict(modelRP,df_test)
predict.prob_rp<-predict.prob_rp[,2]
prediction_rp <- predict.prob_rp >P #×× ××
actual_rp <- df_test$is.Late
cf_rp <- table(prediction_rp,actual_rp)
precision_rp <- cf_rp[2,2]/(cf_rp[2,2] + cf_rp[1,2])
recall_rp <- cf_rp[2,2]/(cf_rp[2,2] + cf_rp[2,1])
cf_rp
precision_rp
recall_rp
F1.DT<-F1_Score(actual_rp, predict.prob_rp, positive = '1')

rocDT.both <-roc(df_test$is.Late , predict.prob_rp, direction = "<", levels = c('0','1'))
auc(rocDT.both)
DT_both<-c("DT","both",precision_rp,recall_rp,auc(rocDT.both),F1.DT)
compare.model.Late<-rbind(compare.model.Late,DT_both)
#----------------------------------------------------smote
df_trainRP<-smote
modelRP <- rpart(is.Late~.,df_trainRP)
rpart.plot(modelRP, box.palette = "RdBu", shadow.col = "grey", nn=TRUE)
predict.prob_rp <- predict(modelRP,df_test)
predict.prob_rp<-predict.prob_rp[,2]
prediction_rp <- predict.prob_rp >P #×× ××
actual_rp <- df_test$is.Late
cf_rp <- table(prediction_rp,actual_rp)
precision_rp <- cf_rp[2,2]/(cf_rp[2,2] + cf_rp[1,2])
recall_rp <- cf_rp[2,2]/(cf_rp[2,2] + cf_rp[2,1])
cf_rp
precision_rp
recall_rp
F1.DT<-F1_Score(actual_rp, predict.prob_rp, positive = '1')

rocDT.smote <-roc(df_test$is.Late , predict.prob_rp, direction = "<", levels = c('0','1'))
auc(rocDT.smote)
DT_smote<-c("DT","smote",precision_rp,recall_rp,auc(rocDT.smote),F1.DT)
compare.model.Late<-rbind(compare.model.Late,DT_smote)

#-----------------------------------------------------------------------------
##                ROC compare: RF DT NB XGboost ADABoost                    ##
#-----------------------------------------------------------------------------
#----
par(mfrow=c(2,1))
#NB
plot(rocNB.clean, col = 'red', main = 'ROC NB')
par(new=TRUE)
plot(rocNB.over, col = 'blue', main = 'ROC NB')
par(new=TRUE)
plot(rocNB.under, col = 'green', main = 'ROC NB')
par(new=TRUE)
plot(rocNB.SMOTE, col = 'pink', main = 'ROC NB')
par(new=TRUE)
plot(rocNB.both, col = 'orange', main = 'ROC NB')
#DT
plot(rocDT.clean, col = 'red', main = 'ROC DT')
par(new=TRUE)
plot(rocDT.over, col = 'blue', main = 'ROC DT')
par(new=TRUE)
plot(rocDT.under, col = 'green', main = 'ROC DT')
par(new=TRUE)
plot(rocDT., col = 'pink', main = 'ROC DT')
par(new=TRUE)
plot(rocDT.smote, col = 'orange', main = 'ROC DT')
#RF
plot(perf.RF.clean,main = "ROC RF",col=5,lwd=2)
par(new=TRUE)
plot(perf.RF.both,main = "ROC RF",col=2,lwd=2)#red
par(new=TRUE)
plot(perf.RF.SMOTE,main = "ROC RF",col=1,lwd=2)#black
par(new=TRUE)
plot(perf.RF.over,main = "ROC RF",col=3,lwd=2)#green
par(new=TRUE)
plot(perf.RF.under,main = "ROC RF",col=4,lwd=2)#blue
#XG
plot(rocXG.clean, col = 'red', main = 'ROC XGBOOST')
par(new=TRUE)
plot(rocXG.over, col = 'blue', main = 'ROC XGBOOST')
par(new=TRUE)
plot(rocXG.under, col = 'green', main = 'ROC XGBOOST')
par(new=TRUE)
plot(rocXG.SMOTE, col = 'pink', main = 'ROC XGBOOST')
par(new=TRUE)
plot(rocXG.both, col = 'orange', main = 'ROC XGBOOST')
#ADA
#plot(rocADA.clean, col = 'red', main = 'ROC ADABOOST')
#par(new=TRUE)
#plot(rocADA.over, col = 'blue', main = 'ROC ADABOOST')
#par(new=TRUE)
#plot(rocADA.under, col = 'green', main = 'ROC ADABOOST')
#par(new=TRUE)
#plot(rocADA.SMOTE, col = 'pink', main = 'ROC ADABOOST')
#par(new=TRUE)
#plot(rocADA.both, col = 'orange', main = 'ROC ADABOOST')
#compare.model<-as.data.frame(compare.model)
#plot(compare.model$Model,compare.model$AUC, main = 'AUC')

View(compare.model.Late)
#-------
compare.model<-compare.model[-1,]#remove the inicial column 
compare.model$fullname<-paste(compare.model$Model,compare.model$`Data solution`)#add column just to combine model and data types
compare.model
#find top 3 models
bestM<-top_n(compare.model,5,AUC)
bestM<-rbind(bestM,top_n(compare.model,5,Precision))
bestM<-rbind(bestM,top_n(compare.model,5,Recall))
table(bestM$fullname)
compare.model
#compare the diffrents solutions to the imballenced data
library(gridExtra)
p1<-ggplot(compare.model, aes(compare.model$`Data solution`,compare.model$Precision)) + geom_col()
p2<-ggplot(compare.model, aes(compare.model$`Data solution`,compare.model$Recall)) + geom_col()
p3<-ggplot(compare.model, aes(compare.model$`Data solution`,compare.model$AUC)) + geom_col()
grid.arrange(p1,p2,p3)
#compare the diffrents Models
p4<-ggplot(compare.model, aes(compare.model$Model,compare.model$Precision)) + geom_col()
p5<-ggplot(compare.model, aes(compare.model$Model,compare.model$Recall)) + geom_col()
p6<-ggplot(compare.model, aes(compare.model$Model,compare.model$AUC)) + geom_col()
grid.arrange(p4,p5,p6)
#
top_n(compare.model,10,Recall)


#####----------------####
###     HOTFIX        ###
####-----------------####

str(df)
df$DropType<-NULL
df$Generation<-NULL
df$IpFamily<-NULL
df_rf<-df
set.seed(123)
filter <- sample.split(df_rf$Drop , SplitRatio = 0.7)#????????
df_train <- subset(df_rf, filter ==T)
df_test <- subset(df_rf, filter ==F)
#Oversampeling
over<- ovun.sample(is.hotfix~. ,data=df_train, method='over' , N= 32500)$data
#undersampling
under<- ovun.sample(is.hotfix~. ,data=df_train, method='under' , N= 2100)$data
#both
both<- ovun.sample(is.hotfix~. ,data=df_train, method='both' , N=17000)$data
#smote
smote<-SMOTE(is.hotfix~ ., df_train, perc.over = 1500, k = 5, perc.under = 500)

table(df_train$is.hotfix)
table(over$is.hotfix)
table(under$is.hotfix)
table(both$is.hotfix)
table(smote$is.hotfix)


#---------------------------------------------------------------------------------------------------#-


# Random hotfix
######
df_trainRF<-df_train

modelRF <- randomForest(is.hotfix~.,data = df_trainRF , importance=TRUE, na.action=na.exclude)
prediction.RF<-predict(modelRF,df_test)
actual.rf <- df_test$is.hotfix
cf.rf <- table(prediction.RF,actual.rf)
precision.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[2,1])
recall.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[1,2])
cf.rf 
precision.rf
recall.rf
#####
table(df$is.hotfix)
#----------------------------------------------------
#solutions for imbalanced Data 
#----------------------------------------------------


#---------------
#   Random     #
#---------------
##model random target late
#####
#data raw

####
#data over
####
df_trainRF<-over
modelRF <- randomForest(is.hotfix~.,data = df_trainRF , importance=TRUE, na.action=na.exclude)
prediction.RF<-predict(modelRF,df_test)
actual.rf <- df_test$is.hotfix
cf.rf <- table(prediction.RF,actual.rf)
precision.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[2,1])
recall.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[1,2])
cf.rf 
precision.rf
recall.rf
###
#data under
###
df_trainRF<-under
modelRF <- randomForest(is.hotfix~.,data = df_trainRF , importance=TRUE, na.action=na.exclude)
prediction.RF<-predict(modelRF,df_test)
actual.rf <- df_test$is.hotfix
cf.rf <- table(prediction.RF,actual.rf)
precision.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[2,1])
recall.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[1,2])
cf.rf 
precision.rf
recall.rf
###
#data both
###
df_trainRF<-both
modelRF <- randomForest(is.hotfix~.,data = df_trainRF , importance=TRUE, na.action=na.exclude)
prediction.RF<-predict(modelRF,df_test)
actual.rf <- df_test$is.hotfix
cf.rf <- table(prediction.RF,actual.rf)
precision.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[2,1])
recall.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[1,2])
cf.rf 
precision.rf
recall.rf
####
#data smote
####
df_trainRF<-smote
modelRF <- randomForest(is.hotfix~.,data = df_trainRF , importance=TRUE, na.action=na.exclude)
prediction.RF<-predict(modelRF,df_test)
actual.rf <- df_test$is.hotfix
cf.rf <- table(prediction.RF,actual.rf)
precision.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[2,1])
recall.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[1,2])
cf.rf 
precision.rf
recall.rf
#####

#----------------#
#       NB       #
#----------------#
#Naive Bais
#####
library(e1071)
#the prediction probebility P
P<-0.5
df_nb<-both
samp_size = nrow(df_nb[df_nb$is.hotfix==1,])
table(df_nb$is.hotfix)
dim(df_nb)
d<-dim(df_nb)[2]

model_NB <- naiveBayes(df_nb[,-2],df_nb$is.hotfix)
prediction_NB <- predict(model_NB,df_test[,-2], type = 'raw')
prediction_bankrupt_NB <- prediction_NB[,"1"]
actual_NB <- df_test$is.hotfix
predicted_NB <- prediction_bankrupt_NB > P #×× ××
cf_NB <- table(predicted_NB,actual_NB)
precision_NB <- cf_NB[2,2]/(cf_NB[2,2] + cf_NB[1,2])
recall_NB <- cf_NB[2,2]/(cf_NB[2,2] + cf_NB[2,1])
cf_NB
precision_NB
recall_NB
rocNB.clean <-roc(df_test$is.hotfix , prediction_bankrupt_NB, direction = "<", levels = c('0','1'))
auc(rocNB.clean)

summary(model_NB)

###Over

df_nb<-over
samp_size = nrow(df_nb[df_nb$is.hotfix==1,])
table(df_nb$is.hotfix)
dim(df_nb)
d<-dim(df_nb)[2]

model_NB <- naiveBayes(df_nb[,-2],df_nb$is.hotfix)
prediction_NB <- predict(model_NB,df_test[,-2], type = 'raw')
prediction_bankrupt_NB <- prediction_NB[,"1"]
actual_NB <- df_test$is.hotfix
predicted_NB <- prediction_bankrupt_NB > P #×× ××
cf_NB <- table(predicted_NB,actual_NB)
precision_NB <- cf_NB[2,2]/(cf_NB[2,2] + cf_NB[1,2])
recall_NB <- cf_NB[2,2]/(cf_NB[2,2] + cf_NB[2,1])
cf_NB
precision_NB
recall_NB
rocNB.clean <-roc(df_test$is.hotfix , prediction_bankrupt_NB, direction = "<", levels = c('0','1'))
auc(rocNB.clean)

summary(model_NB)
##under

df_nb<-under
samp_size = nrow(df_nb[df_nb$is.hotfix==1,])
table(df_nb$is.hotfix)
dim(df_nb)
d<-dim(df_nb)[2]

model_NB <- naiveBayes(df_nb[,-2],df_nb$is.hotfix)
prediction_NB <- predict(model_NB,df_test[,-2], type = 'raw')
prediction_bankrupt_NB <- prediction_NB[,"1"]
actual_NB <- df_test$is.hotfix
predicted_NB <- prediction_bankrupt_NB > P #×× ××
cf_NB <- table(predicted_NB,actual_NB)
precision_NB <- cf_NB[2,2]/(cf_NB[2,2] + cf_NB[1,2])
recall_NB <- cf_NB[2,2]/(cf_NB[2,2] + cf_NB[2,1])
cf_NB
precision_NB
recall_NB
rocNB.clean <-roc(df_test$is.hotfix , prediction_bankrupt_NB, direction = "<", levels = c('0','1'))
auc(rocNB.clean)

summary(model_NB)
###both

df_nb<-both
samp_size = nrow(df_nb[df_nb$is.hotfix==1,])
table(df_nb$is.hotfix)
dim(df_nb)
d<-dim(df_nb)[2]

model_NB <- naiveBayes(df_nb[,-2],df_nb$is.hotfix)
prediction_NB <- predict(model_NB,df_test[,-2], type = 'raw')
prediction_bankrupt_NB <- prediction_NB[,"1"]
actual_NB <- df_test$is.hotfix
predicted_NB <- prediction_bankrupt_NB > P #×× ××
cf_NB <- table(predicted_NB,actual_NB)
precision_NB <- cf_NB[2,2]/(cf_NB[2,2] + cf_NB[1,2])
recall_NB <- cf_NB[2,2]/(cf_NB[2,2] + cf_NB[2,1])
cf_NB
precision_NB
recall_NB
rocNB.clean <-roc(df_test$is.hotfix , prediction_bankrupt_NB, direction = "<", levels = c('0','1'))
auc(rocNB.clean)

summary(model_NB)

###smote

df_nb<-smote
samp_size = nrow(df_nb[df_nb$is.hotfix==1,])
table(df_nb$is.hotfix)
dim(df_nb)
d<-dim(df_nb)[2]

model_NB <- naiveBayes(df_nb[,-2],df_nb$is.hotfix)
prediction_NB <- predict(model_NB,df_test[,-2], type = 'raw')
prediction_bankrupt_NB <- prediction_NB[,"1"]
actual_NB <- df_test$is.hotfix
predicted_NB <- prediction_bankrupt_NB > P #×× ××
cf_NB <- table(predicted_NB,actual_NB)
precision_NB <- cf_NB[2,2]/(cf_NB[2,2] + cf_NB[1,2])
recall_NB <- cf_NB[2,2]/(cf_NB[2,2] + cf_NB[2,1])
cf_NB
precision_NB
recall_NB
rocNB.clean <-roc(df_test$is.hotfix , prediction_bankrupt_NB, direction = "<", levels = c('0','1'))
auc(rocNB.clean)

summary(model_NB)

#####


#XGBoost
#----
#df_rf<-df
#df_rf$key <- NULL
#df_rf$ï..Configuration <- NULL
#df_rf$LeadProduct <- NULL
#df_rf$DropType <- NULL
###
#clean
###
df_xgBoost_hasNA_train<-df_train
df_xgBoost_hasNA_test<-df_test
df_XG<-df_xgBoost_hasNA_train#**************************
df_XG$is.hotfix<-as.integer(df_XG$is.hotfix)-1

#options(na.action = 'na.pass') #############!!!!!!!!!!!!!!!!!!!!!!! alert!!!!!!!
trainm<-sparse.model.matrix(is.hotfix~.-1, data=df_XG)
length(df_XG$is.hotfix)
train_lable<-df_XG[,"is.hotfix"]
train_matrix<-xgb.DMatrix(data=as.matrix(trainm), label = train_lable)
#
df_test_xg<-df_xgBoost_hasNA_test#****************************
df_test_xg$is.hotfix<-as.integer(df_test_xg$is.hotfix)-1
#options(na.action = 'na.fail') #############!!!!!!!!!!!!!!!!!!!!!!! alert!!!!!!!
testm <-sparse.model.matrix(is.hotfix~.-1,data=df_test_xg)
test_lable<- df_test_xg[,"is.hotfix"]
test_matrix<-xgb.DMatrix(data=as.matrix(testm), label = test_lable)
nc <-length(unique(train_lable))
xgb_params<-list("objective" = "multi:softprob","eval_metric" = "mlogloss", "num_class"=nc)
watchlist<-list(train = train_matrix , test= test_matrix)
#test
model.xgb <-xgb.train(params = xgb_params, data = train_matrix, nrounds = 100, watchlist = watchlist,eta=0.5,max.depth = 6,gamma = 0, subsample = 1, colsample_bytree = 1, missing = NA , seed = 55)
e<- data.frame(model.xgb$evaluation_log)
plot(e$iter, e$train_mlogloss, col = 'blue')
lines(e$iter, e$test_mlogloss, col = 'red')
min(e$test_mlogloss)
#feature importance
imp<-xgb.importance(colnames(train_matrix) , model = model.xgb)
print(imp)
xgb.plot.importance(imp)
#predictions
p<-predict(model.xgb, newdata = test_matrix)
aa<-head(p)
aa[3]+ aa[4]
length(p)/2
pred <-matrix(p,nrow=nc,ncol=length(p)/(nc)) %>%
  t()%>%
  data.frame()%>%
  mutate(label = test_lable, max_prob = max.col(.,"last")-1)
head(pred)
cf_xgb<-table(Prediction = pred$max_prob , Actual = pred$label)
precision.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[2,1])
recall.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[1,2])
cf_xgb
precision.xgb
recall.xgb
###
########################over
###
df_xgBoost_hasNA_train<-over
df_xgBoost_hasNA_test<-df_test
df_XG<-df_xgBoost_hasNA_train#**************************
df_XG$is.hotfix<-as.integer(df_XG$is.hotfix)-1

#options(na.action = 'na.pass') #############!!!!!!!!!!!!!!!!!!!!!!! alert!!!!!!!
trainm<-sparse.model.matrix(is.hotfix~.-1, data=df_XG)
length(df_XG$is.hotfix)
train_lable<-df_XG[,"is.hotfix"]
length(train_lable)/length(colnames(train_lable))
train_matrix<-xgb.DMatrix(data=as.matrix(trainm), label = train_lable)
#
df_test_xg<-df_xgBoost_hasNA_test#****************************
df_test_xg$is.hotfix<-as.integer(df_test_xg$is.hotfix)-1
#options(na.action = 'na.fail') #############!!!!!!!!!!!!!!!!!!!!!!! alert!!!!!!!
testm <-sparse.model.matrix(is.hotfix~.-1,data=df_test_xg)
test_lable<- df_test_xg[,"is.hotfix"]
test_matrix<-xgb.DMatrix(data=as.matrix(testm), label = test_lable)
nc <-length(unique(train_lable))
xgb_params<-list("objective" = "multi:softprob","eval_metric" = "mlogloss", "num_class"=nc)
watchlist<-list(train = train_matrix , test= test_matrix)
#test
model.xgb <-xgb.train(params = xgb_params, data = train_matrix, nrounds = 100, watchlist = watchlist,eta=0.5,max.depth = 6,gamma = 0, subsample = 1, colsample_bytree = 1, missing = NA , seed = 55)
e<- data.frame(model.xgb$evaluation_log)
plot(e$iter, e$train_mlogloss, col = 'blue')
lines(e$iter, e$test_mlogloss, col = 'red')
min(e$test_mlogloss)
#feature importance
imp<-xgb.importance(colnames(train_matrix) , model = model.xgb)
print(imp)
xgb.plot.importance(imp)
#predictions
summary(model.xgb)
p<-predict(model.xgb, newdata = test_matrix)
aa<-head(p)
aa[3]+ aa[4]
length(p)/2
pred <-matrix(p,nrow=nc,ncol=length(p)/(nc)) %>%
  t()%>%
  data.frame()%>%
  mutate(label = test_lable, max_prob = max.col(.,"last")-1)
head(pred)
cf_xgb<-table(Prediction = pred$max_prob , Actual = pred$label)
precision.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[2,1])
recall.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[1,2])
cf_xgb
precision.xgb
recall.xgb
###
###########under
###
df_xgBoost_hasNA_train<-under
df_xgBoost_hasNA_test<-df_test
df_XG<-df_xgBoost_hasNA_train#**************************
df_XG$is.hotfix<-as.integer(df_XG$is.hotfix)-1

#options(na.action = 'na.pass') #############!!!!!!!!!!!!!!!!!!!!!!! alert!!!!!!!
trainm<-sparse.model.matrix(is.hotfix~.-1, data=df_XG)
length(df_XG$is.hotfix)
train_lable<-df_XG[,"is.hotfix"]
length(train_lable)/length(colnames(train_lable))
train_matrix<-xgb.DMatrix(data=as.matrix(trainm), label = train_lable)
#
df_test_xg<-df_xgBoost_hasNA_test#****************************
df_test_xg$is.hotfix<-as.integer(df_test_xg$is.hotfix)-1
#options(na.action = 'na.fail') #############!!!!!!!!!!!!!!!!!!!!!!! alert!!!!!!!
testm <-sparse.model.matrix(is.hotfix~.-1,data=df_test_xg)
test_lable<- df_test_xg[,"is.hotfix"]
test_matrix<-xgb.DMatrix(data=as.matrix(testm), label = test_lable)
nc <-length(unique(train_lable))
xgb_params<-list("objective" = "multi:softprob","eval_metric" = "mlogloss", "num_class"=nc)
watchlist<-list(train = train_matrix , test= test_matrix)
#test
model.xgb <-xgb.train(params = xgb_params, data = train_matrix, nrounds = 100, watchlist = watchlist,eta=0.5,max.depth = 6,gamma = 0, subsample = 1, colsample_bytree = 1, missing = NA , seed = 55)
e<- data.frame(model.xgb$evaluation_log)
plot(e$iter, e$train_mlogloss, col = 'blue')
lines(e$iter, e$test_mlogloss, col = 'red')
min(e$test_mlogloss)
#feature importance
imp<-xgb.importance(colnames(train_matrix) , model = model.xgb)
print(imp)
xgb.plot.importance(imp)
#predictions
p<-predict(model.xgb, newdata = test_matrix)
aa<-head(p)
aa[3]+ aa[4]
length(p)/2
pred <-matrix(p,nrow=nc,ncol=length(p)/(nc)) %>%
  t()%>%
  data.frame()%>%
  mutate(label = test_lable, max_prob = max.col(.,"last")-1)
head(pred)
cf_xgb<-table(Prediction = pred$max_prob , Actual = pred$label)
precision.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[2,1])
recall.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[1,2])
cf_xgb
precision.xgb
recall.xgb
###
#both
###
df_xgBoost_hasNA_train<-both
df_xgBoost_hasNA_test<-df_test
df_XG<-df_xgBoost_hasNA_train#**************************
df_XG$is.hotfix<-as.integer(df_XG$is.hotfix)-1

#options(na.action = 'na.pass') #############!!!!!!!!!!!!!!!!!!!!!!! alert!!!!!!!
trainm<-sparse.model.matrix(is.hotfix~.-1, data=df_XG)
length(df_XG$is.hotfix)
train_lable<-df_XG[,"is.hotfix"]
length(train_lable)/length(colnames(train_lable))
train_matrix<-xgb.DMatrix(data=as.matrix(trainm), label = train_lable)
#
df_test_xg<-df_xgBoost_hasNA_test#****************************
df_test_xg$is.hotfix<-as.integer(df_test_xg$is.hotfix)-1
#options(na.action = 'na.fail') #############!!!!!!!!!!!!!!!!!!!!!!! alert!!!!!!!
testm <-sparse.model.matrix(is.hotfix~.-1,data=df_test_xg)
test_lable<- df_test_xg[,"is.hotfix"]
test_matrix<-xgb.DMatrix(data=as.matrix(testm), label = test_lable)
nc <-length(unique(train_lable))
xgb_params<-list("objective" = "multi:softprob","eval_metric" = "mlogloss", "num_class"=nc)
watchlist<-list(train = train_matrix , test= test_matrix)
#test
model.xgb <-xgb.train(params = xgb_params, data = train_matrix, nrounds = 100, watchlist = watchlist,eta=0.5,max.depth = 6,gamma = 0, subsample = 1, colsample_bytree = 1, missing = NA , seed = 55)
e<- data.frame(model.xgb$evaluation_log)
plot(e$iter, e$train_mlogloss, col = 'blue')
lines(e$iter, e$test_mlogloss, col = 'red')
min(e$test_mlogloss)
#feature importance
imp<-xgb.importance(colnames(train_matrix) , model = model.xgb)
print(imp)
xgb.plot.importance(imp)
#predictions
p<-predict(model.xgb, newdata = test_matrix)
aa<-head(p)
aa[3]+ aa[4]
length(p)/2
pred <-matrix(p,nrow=nc,ncol=length(p)/(nc)) %>%
  t()%>%
  data.frame()%>%
  mutate(label = test_lable, max_prob = max.col(.,"last")-1)
head(pred)
cf_xgb<-table(Prediction = pred$max_prob , Actual = pred$label)
precision.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[2,1])
recall.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[1,2])
cf_xgb
precision.xgb
recall.xgb
###
#smote
###
df_xgBoost_hasNA_train<-smote
df_xgBoost_hasNA_test<-df_test
df_XG<-df_xgBoost_hasNA_train#**************************
df_XG$is.hotfix<-as.integer(df_XG$is.hotfix)-1

#options(na.action = 'na.pass') #############!!!!!!!!!!!!!!!!!!!!!!! alert!!!!!!!
trainm<-sparse.model.matrix(is.hotfix~.-1, data=df_XG)
length(df_XG$is.hotfix)
train_lable<-df_XG[,"is.hotfix"]
length(train_lable)/length(colnames(train_lable))
train_matrix<-xgb.DMatrix(data=as.matrix(trainm), label = train_lable)
#
df_test_xg<-df_xgBoost_hasNA_test#****************************
df_test_xg$is.hotfix<-as.integer(df_test_xg$is.hotfix)-1
#options(na.action = 'na.fail') #############!!!!!!!!!!!!!!!!!!!!!!! alert!!!!!!!
testm <-sparse.model.matrix(is.hotfix~.-1,data=df_test_xg)
test_lable<- df_test_xg[,"is.hotfix"]
test_matrix<-xgb.DMatrix(data=as.matrix(testm), label = test_lable)
nc <-length(unique(train_lable))
xgb_params<-list("objective" = "multi:softprob","eval_metric" = "mlogloss", "num_class"=nc)
watchlist<-list(train = train_matrix , test= test_matrix)
#test
model.xgb <-xgb.train(params = xgb_params, data = train_matrix, nrounds = 100, watchlist = watchlist,eta=0.5,max.depth = 6,gamma = 0, subsample = 1, colsample_bytree = 1, missing = NA , seed = 55)
e<- data.frame(model.xgb$evaluation_log)
plot(e$iter, e$train_mlogloss, col = 'blue')
lines(e$iter, e$test_mlogloss, col = 'red')
min(e$test_mlogloss)
#feature importance
imp<-xgb.importance(colnames(train_matrix) , model = model.xgb)
print(imp)
xgb.plot.importance(imp)
#predictions
p<-predict(model.xgb, newdata = test_matrix)
aa<-head(p)
aa[3]+ aa[4]
length(p)/2
pred <-matrix(p,nrow=nc,ncol=length(p)/(nc)) %>%
  t()%>%
  data.frame()%>%
  mutate(label = test_lable, max_prob = max.col(.,"last")-1)
head(pred)
cf_xgb<-table(Prediction = pred$max_prob , Actual = pred$label)
precision.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[2,1])
recall.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[1,2])
cf_xgb
precision.xgb
recall.xgb


























aggr(df)  # shows percentage of missing values in each column
#aggr(df, numbers=TRUE, sortVars=TRUE, labels=names(df), cex.axis=.7, gap=3, ylab=c("Proportion of missingness","Missingness Pattern"))
#GXoost NA
####data = df_xgBoost_hasNA*/*/*/*/*/*/*/*/
#----
df_rf<-df
summary(df_rf)
df_rf$key <- NULL
df_rf$ï..Configuration <- NULL
df_rf$LeadProduct <- NULL
df_rf$DropType <- NULL

filter <- sample.split(df_rf$is.Late , SplitRatio = 0.7)#????????
df_train <- subset(df_rf, filter ==T)
df_test <- subset(df_rf, filter ==F)

#df_train<-dgCMatrix_2scipy_sparse(df_train)
df_XG<-df_train
df_XG$is.Late<-as.integer(df_XG$is.Late)
df_XG$is.hotfix<-as.integer(df_XG$is.hotfix)
str(df_XG)
dim(c)
trainm<-sparse.model.matrix(is.Late~., data=df_XG)
head(trainm)
colnames(trainm)
dim(trainm)
table(df_XG$is.Late)
str(trainm)
length(train_lable)
train_lable<-df_train$is.Late
train_matrix<-xgb.DMatrix(data=as.matrix(trainm), label = train_lable)

df_test_xg<-df_test#****************************
df_test_xg$is.hotfix<-as.integer(df_test_xg$is.hotfix)-1
testm <-sparse.model.matrix(is.hotfix~.-1,data=df_test_xg)
test_lable<- df_test_xg[,"is.hotfix"]
test_matrix<-xgb.DMatrix(data=as.matrix(testm), label = test_lable)

nc <-length(unique(train_lable))
xgb_params<-list("objective" = "multi:softprob","eval_metric" = "mlogloss", "num_class"=nc)
watchlist<-list(train = train_matrix , test= test_matrix)

model.xgb <-xgb.train(params = xgb_params, data = train_matrix, nrounds = 200, watchlist = watchlist,eta=0.76,max.depth = 4,gamma = 0, subsample = 1, colsample_bytree = 1, missing = NA , seed = 55)
e<- data.frame(model.xgb$evaluation_log)
plot(e$iter, e$train_mlogloss, col = 'blue')
lines(e$iter, e$test_mlogloss, col = 'red')
min(e$test_mlogloss)
#feature importance
imp<-xgb.importance(colnames(train_matrix) , model = model.xgb)
print(imp)
xgb.plot.importance(imp)
#predictions
p<-predict(model.xgb, newdata = test_matrix)
aa<-head(p)
aa[3]+ aa[4]
length(p)/2
pred <-matrix(p,nrow=nc,ncol=length(p)/(nc)) %>%
  t()%>%
  data.frame()%>%
  mutate(label = test_lable, max_prob = max.col(.,"last")-1)
head(pred)
cf_xgb<-table(Prediction = pred$max_prob , Actual = pred$label)
precision.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[2,1])
recall.xgb <- cf_xgb[2,2]/(cf_xgb[2,2] + cf_xgb[1,2])
cf_xgb
precision.xgb
recall.xgb
rocXG.clean <-roc(df_test$class , pred$max_prob, direction = "<", levels = c('0','1'))
auc(rocXG.clean)
XG_clean<-c("XGBoost","clean",precision.xgb,recall.xgb,auc(rocXG.clean))
compare.model<-rbind(compare.model,XG_clean)

































#data.corr data with only integer
#----

data.corr<-df
str(data.corr)

data.corr$Configuration<-NULL
data.corr$IpConfigurationId<-NULL
data.corr$LeadProduct<-NULL
data.corr$X<-NULL
data.corr$LeadProductId<-NULL
data.corr$LeadProduct.Type<-NULL
data.corr$IP.Category<-NULL
data.corr$IpFamily<-NULL
data.corr$IpFamilyId<-NULL
data.corr$Generation<-NULL
data.corr$IpGenerationId<-NULL
data.corr$Lifecycle<-NULL
data.corr$DevGroup<-NULL
data.corr$IpScheduleContacts<-NULL
data.corr$Process<-NULL
data.corr$Node<-NULL
data.corr$Reuse<-NULL
data.corr$IrrId<-NULL
data.corr$PlanningStatus<-NULL
data.corr$IpType<-NULL
data.corr$IpgPms<-NULL
data.corr$IpScheduleContacts<-NULL
data.corr$IpDevelopmentLead<-NULL
data.corr$MilestoneId<-NULL
data.corr$Revision<-NULL
data.corr$TrendYYYYWW<-as.integer(data.corr$TrendYYYYWW)
data.corr$QOV_status<-NULL
data.corr$Drop<-NULL
data.corr$DropType<-NULL
data.corr$cluster_or_ip<-NULL
data.corr$IsSubIp<-NULL
data.corr$PorYYYYWW<-NULL
data.corr$TrendYYYYWW<-NULL
data.corr$WW<-NULL
data.corr$is.missing.trend<-NULL
data.corr$is.missing.por<-NULL


str(data.corr)
data.corr<-as.data.frame(data.corr)
typeof(data.corr)

install.packages("Hmisc")
library("Hmisc")
rcorr(data.corr, type = c("pearson","spearman"))

df_cor <- cor(data.corr[1:(ncol(data.corr)-1)], use = "complete.obs")
df_cor
corrplot(df_cor, )
#removing one of the for each pair of fields that have a correlation of 0.8 and up
hc <- findCorrelation(df_cor, cutoff=0.8, verbose = T)
hc = sort(hc)
hc

df_cor <- cor(df[1:(ncol(df)-1)], use = "complete.obs")
df_cor
corrplot(df_cor, type="upper")
#----
#ggplot correlations
ggplot(df, aes(df$is.Late,df$DropType)) + geom_bar()# + ylim(0.75,1)
ggplot(df, aes(df$is.hotfix,df$QOV_Grade)) + geom_boxplot() + ylim(0.75,1)
ggplot(df, aes(df$is.hotfix,df$Coverage_Grade)) + geom_boxplot()# + ylim(0.75,1)
ggplot(df, aes(df$is.hotfix,df$X..of.PCRs.post.IP3)) + geom_boxplot()# + ylim(0.75,1)
ggplot(df, aes(df$is.hotfix,df$ipds_avg_score)) + geom_boxplot()# + ylim(0.75,1)
ggplot(df, aes(df$is.hotfix,df$Regression_Grade)) + geom_boxplot()# + ylim(0.75,1)
ggplot(df, aes(df$is.hotfix,df$X..of.post.si.bugs)) + geom_boxplot()# + ylim(0.75,1)


ggplot(df) + aes(x = df$Drop, fill = df$is.hotfix) + geom_bar()
ggplot(df) + aes(x = df$Drop, fill = df$is.hotfix) + geom_bar(position = "fill")

ggplot(df) + aes(x = df$LeadProduct, fill = df$is.hotfix) + geom_bar(position = "fill")
ggplot(df) + aes(x = df$LeadProduct, fill = df$is.hotfix) + geom_bar()

ggplot(df) + aes(x = df$IpFamily, fill = df$is.hotfix) + geom_bar(position = "fill")
ggplot(df) + aes(x = df$IpFamily, fill = df$is.hotfix) + geom_bar()

ggplot(df) + aes(x = df$DropType, fill = df$is.Late) + geom_bar(position = "fill")
ggplot(df) + aes(x = df$DropType, fill = df$is.Late) + geom_bar()

ggplot(df) + aes(x = df$Node, fill = df$is.hotfix) + geom_bar(position = "fill")
ggplot(df) + aes(x = df$Node, fill = df$is.hotfix) + geom_bar()

ggplot(df) + aes(x = df$IsTC, fill = df$is.hotfix) + geom_bar(position = "fill")
ggplot(df) + aes(x = df$IsTC, fill = df$is.hotfix) + geom_bar()

ggplot(df) + aes(x = df$IpType, fill = df$is.hotfix) + geom_bar(position = "fill")
ggplot(df) + aes(x = df$IpType, fill = df$is.hotfix) + geom_bar()

ggplot(df) + aes(x = df$IpgPms, fill = df$is.hotfix) + geom_bar(position = "fill")
ggplot(df) + aes(x = df$IpgPms, fill = df$is.hotfix) + geom_bar()

ggplot(df) + aes(x = df$X..of.post.si.bugs, fill = df$is.hotfix) + geom_bar(position = "fill")
ggplot(df) + aes(x = df$X..of.post.si.bugs, fill = df$is.hotfix) + geom_bar()

ggplot(df) + aes(x = df$Lifecycle, fill = df$is.hotfix) + geom_bar(position = "fill")
ggplot(df) + aes(x = df$Lifecycle, fill = df$is.hotfix) + geom_bar()

ggplot(df) + aes(x = df$Regression_Grade, fill = df$is.hotfix) + geom_bar(position = "fill")
ggplot(df) + aes(x = df$Lifecycle, fill = df$is.hotfix) + geom_bar()

ggplot(df) + aes(x = df$Lifecycle, fill = df$is.hotfix) + geom_bar(position = "fill")
ggplot(df) + aes(x = df$Lifecycle, fill = df$is.hotfix) + geom_bar()

#chi Square categorial correlation
library("gplots")
# 1. convert the data as a table
df.onlyFactors<-df
str(df)
df.onlyFactors$X..of.Fonctional.PCRs.post.IP3<-NULL
df.onlyFactors$X..of.PCRs.post.IP3<-NULL
df.onlyFactors$sla.resolution<-NULL
df.onlyFactors$sla.response<-NULL
df.onlyFactors$X..of.post.si.bugs<-NULL
df.onlyFactors$ipds_avg_score<-NULL
df.onlyFactors$WW.before.milestone<-NULL
df.onlyFactors$QOV.diff<-NULL
df.onlyFactors$Validation_Plan_Grade<-NULL
df.onlyFactors$Regression_Grade<-NULL
df.onlyFactors$Coverage_Grade<-NULL
df.onlyFactors$qov_ww<-NULL
df.onlyFactors$QOV_target<-NULL
df.onlyFactors$QOV_Grade<-NULL
df.onlyFactors$WW<-NULL
df.onlyFactors$Trend.Year<-NULL
df.onlyFactors$PorYYYYWW<-NULL
df.onlyFactors$TrendYYYYWW<-NULL
df.onlyFactors$LeadProductId<-NULL
dt <- as.table(as.matrix(df.onlyFactors))
# 2. Graph
balloonplot(t(dt), main ="df.onlyFactors", xlab ="", ylab="",
            label = FALSE, show.margins = FALSE)
library("graphics")
mosaicplot(dt, shade = TRUE, las=2,
           main = "housetasks")

#chi^2 option 2
chisq.test(df$is.on.time, df$Drop, correct=FALSE)

#information gain
install.packages("Boruta")
install.packages("ranger")
install.packages("mlbench")
library(Boruta)
library(mlbench)
library(caret)
library(randomForest)
boruta<-Boruta(df$is.hotfix ~. , data =df)
####labeling###
#----


dfg<-as.data.frame(intel.prepare$cleanConfigs)
dfg$Configuration<-intel.prepare$Configuration
unique(dfg[,2])
i<-as.character(c((1:length(intel.prepare$is.missing.por))))
typeof(i)
y<-as.factor(paste('Y.',i))#this is for the column names

x<-as.character(c(1:length(unique(intel.prepare$Configuration))))
#https://www.r-bloggers.com/string-concatenation-in-r/
C<-as.character(paste('C.',x))#configurations
#install.packages('CatEncoders')
#library(CatEncoders)

#lenc<-LabelEncoder.fit(as.character(intel.prepare$Configuration))
#z <- transform(lenc,C)
#inverse.transform(lenc,z)
#install.packages('plyr')
#library(plyr)
##http://www.cookbook-r.com/Manipulating_data/Renaming_levels_of_a_factor/
configs<-as.character(mapvalues(intel.prepare$Configuration, from = unique(intel.prepare$Configuration), to = C))

configs.df<-as.data.frame(congifs)
configs.df$Configuration<-intel.prepare$Configuration
print(unique(configs.df))

library(dplyr)
try<-left_join(intel.prepare, configs.df, by = c("Configuration"="congifs"))
#----




#----

###Cost sensative##https://cran.r-project.org/web/packages/penalized/penalized.pdf
#install.packages('penalized')
#library('penalized')
#pen<-penalized(as.factor(df_train$class), penalized=0.5, unpenalized=~0, lambda1=0,
#          lambda2=0, positive = FALSE, df_train, fusedl=FALSE,
#         model = c("cox", "logistic", "linear", "poisson"),
#        startbeta=0.1, startgamma=0.2, steps =1, epsilon = 1e-10,
#       maxiter=25, standardize = FALSE, trace = TRUE)


####----------------#
#cost sensative*/*/**/*/
#----
library(data.table)
install.packages('mlr')
library(mlr)
df_dtTrain<-as.data.table(df_train)
df_dtTest<-as.data.table(df_test)
#
table(is.na(df_dtTrain))
labels <- as.numeric(df_dtTrain$class)-1
ts_label <- as.numeric(df_dtTrain$class)-1
dtrain <- xgb.DMatrix(data = df_dtTrain,label = labels)
dtest <- xgb.DMatrix(data = new_ts,label=ts_label)
##########cost sensative
# th<-1/6
# w <- (1 - th)/th
# w
# lrn = makeLearner("classif.multinom", trace = FALSE)
# lrn = makeWeightedClassesWrapper(lrn, wcw.weight = w)
# lrn
# costs = matrix(c(0, 1, 5, 0), 2)
# credit.costs = makeCostMeasure(id = "credit.costs", name = "Credit costs", costs = costs,
#                                best = 0, worst = 5)
# credit.task = makeClassifTask(data = df_train_over, target = "class")
# rin = makeResampleInstance("CV", iters = 3, task = credit.task)
# r = resample(lrn, credit.task, rin, measures = list(credit.costs, mmce), show.info = FALSE)
# r
#
#
# lrn = makeLearner("classif.multinom", trace = FALSE)
# lrn = makeWeightedClassesWrapper(lrn)
# ps = makeParamSet(makeDiscreteParam("wcw.weight", seq(4, 12, 0.5)))
# ctrl = makeTuneControlRandom()
# tune.res = tuneParams(lrn, task, resampling = rin, par.set = ps,
#                       measures = list(task, mmce), control = ctrl, show.info = FALSE)
#----
-------------------
  
  ###################cross validation*/*/*/*/*/*/*/*
  -------------------
  #----
install.packages('mlr')
library(mlr)
task = makeClassifTask(data = df_train_over, target = "class")
rdesc = makeResampleDesc("CV", iters = 5)
r = resample(makeLearner("classif.qda"), task, rdesc)
print(r$aggr)
print(r$measures.test)

rdesc = makeResampleDesc("CV", iters = 2, predict = "both")
r = resample(makeLearner("classif.qda"), task, rdesc,measures = list(mmce, setAggregation(mmce, train.mean)))
print(r$aggr)

#----
str(df_rf)
df_rf<-df
df_rf$Configuration<-NULL
df_rf$cluster_or_ip<-NULL
df_rf$Revision<-NULL
df_rf$MilestoneId<-NULL
df_rf$IpDevelopmentLead<-NULL
df_rf$IpGenerationId<-NULL
df_rf$Generation<-NULL
df_rf$IpFamily<-NULL
df_rf$IpFamilyId<-NULL
df_rf$IpConfigurationId<-NULL
df_rf$LeadProduct<-NULL


df$PorYYYYWW<-NULL
filter <- sample.split(df_rf$IP.Category , SplitRatio = 0.7)#????????
df_train <- subset(df_rf, filter ==T)
df_test <- subset(df_rf, filter ==F)
df_trainRF<-df_train
modelRF <- randomForest(is.hotfix~.,data = df_trainRF, importance=TRUE)
prediction.RF<-predict(modelRF,df_test)
actual.rf <- df_test$is.hotfix
cf.rf <- table(prediction.RF,actual.rf)
precision.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[2,1])
recall.rf <- cf.rf[2,2]/(cf.rf[2,2] + cf.rf[1,2])
cf.rf
precision.rf
recall.rf
#AUC for Random Forest
predict.prob.rf <- predict(modelRF,type="prob",df_test)[,2]
library(ROCR)
Pred.Prob.RF<-prediction(predict.prob.rf,df_test$class)
perf.RF.clean<-performance(Pred.Prob.RF,"tpr","fpr")
plot(perf.RF.clean,main = "ROC Curve for Random Forest",col=3,lwd=2)
#-----
P<-0.5
#----------------------------------------------------clean

summary(df)
df$DropType<-NULL
df$TrendYYYYWW<-NULL
filter <- sample.split(df$IpConfigurationId , SplitRatio = 0.7)#????????
df_xgBoost_hasNA_train <- subset(df, filter ==T)
df_xgBoost_hasNA_test <- subset(df, filter ==F)
df_test<-df_xgBoost_hasNA_test
modelRP <- rpart(is.hotfix~.,df_xgBoost_hasNA_train)
rpart.plot(modelRP, box.palette = "RdBu", shadow.col = "grey", nn=TRUE)
predict.prob_rp <- predict(modelRP,df_test)
predict.prob_rp<-predict.prob_rp[,2]
prediction_rp <- predict.prob_rp >P #????????
actual_rp <- df_test$is.hotfix
cf_rp <- table(prediction_rp,actual_rp)
precision_rp <- cf_rp[2,2]/(cf_rp[2,2] + cf_rp[1,2])
recall_rp <- cf_rp[2,2]/(cf_rp[2,2] + cf_rp[2,1])
cf_rp
precision_rp
recall_rp
rocRP.clean <-roc(df_test$class , predict.prob_rp, direction = "<", levels = c('0','1'))
auc(rocRP.clean)
DT_clean<-c("DT","clean",precision_rp,recall_rp,auc(rocRP.clean))
compare.model<-rbind(compare.model,DT_clean)









#PCR time series
setwd("C:/Users/nadavste/Desktop/ml")
pcr.df<-read.csv('pcrHSIO-ICL.csv')
summary(pcr.df)
str(pcr.df)
pcr.df$MID<-as.factor(pcr.df$MID)


